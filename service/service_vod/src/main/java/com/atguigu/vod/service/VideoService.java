package com.atguigu.vod.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface VideoService {

    String uploadVideo(MultipartFile file) throws IOException;

    void removeVideoById(String vodId);

    void removeVodListByIds(List vodList);
}
