package com.atguigu.vod.controller;


import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthRequest;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthResponse;
import com.atguigu.commonutils.R;
import com.atguigu.servicebase.exceptionHandler.GuliException;
import com.atguigu.vod.Utils.ConstantPropertiesUtil;
import com.atguigu.vod.Utils.InitVodclient;
import com.atguigu.vod.service.VideoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Api(description="阿里云视频点播微服务")
@RequestMapping("/eduVod/video")
@RestController
//@CrossOrigin //跨域
public class VideoAdminController {

    @Autowired
    private VideoService videoService;

    @PostMapping("upload")
    public R uploadVideo(
            @ApiParam(name = "file", value = "文件", required = true)
            @RequestParam("file") MultipartFile file) throws Exception {

        String videoId = videoService.uploadVideo(file);
        return R.ok().message("视频上传成功").data("videoId", videoId);
    }

    @DeleteMapping("removeVodById/{vodId}")
    public R removeVodById(
            @ApiParam(name = "id", value = "视频Id", required = true)
            @PathVariable("vodId") String  vodId
    ){
        videoService.removeVideoById(vodId);
        return R.ok().message("视频删除成功");
    }

    @DeleteMapping("removeVodListByIds")
    public R removeVodListByIds(
            @ApiParam(name = "id", value = "视频Id", required = true)
            @RequestParam("vodList") List<String>  vodList
    ){
        videoService.removeVodListByIds(vodList);
        return R.ok().message("视频删除成功");
    }

    //获取播放地址
    @GetMapping("playVideoByCredentials/{videoId}")
    public R playVideoByCredentials(@PathVariable String videoId){
        try {
            DefaultAcsClient client =  InitVodclient.initVodClient(ConstantPropertiesUtil.ACCESS_KEY_ID,ConstantPropertiesUtil.ACCESS_KEY_SECRET);
            //请求
            GetVideoPlayAuthRequest request = new GetVideoPlayAuthRequest();
            request.setVideoId(videoId);

            //响应
            GetVideoPlayAuthResponse response = client.getAcsResponse(request);
            //得到播放凭证
            String playAuth = response.getPlayAuth();

            //返回结果
            return R.ok().message("获取凭证成功").data("playAuth", playAuth);
        } catch (ClientException e) {
            throw new GuliException(20001,"获取视频失败");
        }
    }
}
