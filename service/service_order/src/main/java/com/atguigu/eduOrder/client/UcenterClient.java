package com.atguigu.eduOrder.client;

import com.atguigu.commonutils.orderVo.MemberOrderVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Component
@FeignClient("service-ucenter")
public interface UcenterClient {

    @PostMapping("/eduCenter/member/getInfoUc/{id}")
    MemberOrderVo getInfoUc(@PathVariable("id") String id);
}
