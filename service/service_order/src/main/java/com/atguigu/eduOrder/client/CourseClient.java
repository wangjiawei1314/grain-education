package com.atguigu.eduOrder.client;

import com.atguigu.commonutils.orderVo.CourseOrderVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient("service-edu")
public interface CourseClient {

    //获取课程信息
    @GetMapping("/eduService/edu-course/getCourseOrderInfo/{courseId}")
    CourseOrderVo getCourseOrderInfo(@PathVariable("courseId") String courseId);
}
