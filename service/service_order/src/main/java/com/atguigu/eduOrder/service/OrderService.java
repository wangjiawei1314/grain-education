package com.atguigu.eduOrder.service;

import com.atguigu.eduOrder.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单 服务类
 * </p>
 *
 * @author testjava
 * @since 2020-11-08
 */
public interface OrderService extends IService<Order> {

    String saveOrder(String courseId, String userId);
}
