package com.atguigu.eduOrder.controller;


import com.atguigu.commonutils.JwtUtils;
import com.atguigu.commonutils.R;
import com.atguigu.eduOrder.entity.Order;
import com.atguigu.eduOrder.service.OrderService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 订单 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2020-11-08
 */
@RestController
@RequestMapping("/eduOrder/order")
//@CrossOrigin
public class OrderController {

    @Autowired
    private OrderService orderService;

    //生成订单
    @PostMapping("saveOrderByCourseId/{courseId}")
    public R saveOrderByCourseId(@PathVariable String courseId, HttpServletRequest request){
        String userId = JwtUtils.getMemberIdByJwtToken(request);
        //生成订单
        String orderId =  orderService.saveOrder(courseId,userId);
        return R.ok().data("orderId", orderId);
    }

    @GetMapping("getOrder/{orderId}")
    public R get(@PathVariable String orderId) {
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.eq("order_no",orderId);
        Order order = orderService.getOne(wrapper);
        return R.ok().data("item", order);
    }

    //根据课程的Id和用户Id查询订单状态是否已支付
    @GetMapping("isBuyCourse/{courseId}/{memberId}")
    public Boolean isBuyCourse(@PathVariable String courseId,@PathVariable String  memberId){
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id",courseId);
        wrapper.eq("member_id",memberId);
        wrapper.eq("status",1);
        int result = orderService.count(wrapper);
        if(result>0){
            return true;
        }else{
            return false;
        }
    }
}

