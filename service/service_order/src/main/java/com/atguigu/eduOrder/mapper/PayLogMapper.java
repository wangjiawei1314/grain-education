package com.atguigu.eduOrder.mapper;

import com.atguigu.eduOrder.entity.PayLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 支付日志表 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-11-08
 */
public interface PayLogMapper extends BaseMapper<PayLog> {

}
