package com.atguigu.eduOrder.service.impl;

import com.atguigu.commonutils.orderVo.CourseOrderVo;
import com.atguigu.commonutils.orderVo.MemberOrderVo;
import com.atguigu.eduOrder.Utils.OrderNoUtil;
import com.atguigu.eduOrder.client.CourseClient;
import com.atguigu.eduOrder.client.UcenterClient;
import com.atguigu.eduOrder.entity.Order;
import com.atguigu.eduOrder.mapper.OrderMapper;
import com.atguigu.eduOrder.service.OrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-11-08
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {

    @Autowired
    private UcenterClient ucenter;

    @Autowired
    private CourseClient course;

    @Override
    public String saveOrder(String courseId, String userId) {
        //根据用户Id 查询用户的信息
        MemberOrderVo member =  ucenter.getInfoUc(userId);
        //根据课程Id 查询课程的信息
        CourseOrderVo courseOrder = course.getCourseOrderInfo(courseId);
        //创建订单
        Order order = new Order();
        order.setOrderNo(OrderNoUtil.getOrderNo());
        order.setCourseId(courseId);
        order.setCourseTitle(courseOrder.getTitle());
        order.setCourseCover(courseOrder.getCover());
        order.setTeacherName("test");
        order.setTotalFee(courseOrder.getPrice());
        order.setMemberId(userId);
        order.setMobile(member.getMobile());
        order.setNickname(member.getNickname());
        order.setStatus(0);
        order.setPayType(1);
        baseMapper.insert(order);

        return order.getOrderNo();
    }
}
