package com.atguigu.eduCms.service;

import com.atguigu.eduCms.entity.CrmBanner;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 首页banner表 服务类
 * </p>
 *
 * @author wjw
 * @since 2020-10-29
 */
public interface CrmBannerService extends IService<CrmBanner> {

    List<CrmBanner> selectList(QueryWrapper queryWrapper);
}
