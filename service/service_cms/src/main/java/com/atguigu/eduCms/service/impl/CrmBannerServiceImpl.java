package com.atguigu.eduCms.service.impl;

import com.atguigu.eduCms.entity.CrmBanner;
import com.atguigu.eduCms.mapper.CrmBannerMapper;
import com.atguigu.eduCms.service.CrmBannerService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 首页banner表 服务实现类
 * </p>
 *
 * @author wjw
 * @since 2020-10-29
 */
@Service
public class CrmBannerServiceImpl extends ServiceImpl<CrmBannerMapper, CrmBanner> implements CrmBannerService {

    @Override
    public List<CrmBanner> selectList(QueryWrapper queryWrapper) {
        List<CrmBanner>  list =baseMapper.selectList(queryWrapper);
        return list;
    }
}
