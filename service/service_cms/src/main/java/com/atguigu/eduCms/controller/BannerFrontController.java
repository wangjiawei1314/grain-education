package com.atguigu.eduCms.controller;

import com.atguigu.commonutils.R;
import com.atguigu.eduCms.entity.CrmBanner;
import com.atguigu.eduCms.service.CrmBannerService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 首页banner表 前端控制器
 * </p>
 *
 * @author wjw
 * @since 2020-10-29
 */
@RestController
@RequestMapping("/eduCms/banner-front")
//@CrossOrigin
public class BannerFrontController {

    @Autowired
    private CrmBannerService crmBannerService;

    @Cacheable(value = "banners",key = "'selectIndexList'")
    @GetMapping("getAllBanner")
    public R getAllBanner(){
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("id");
        queryWrapper.last("limit 2");
        List<CrmBanner>  banneres = crmBannerService.selectList(queryWrapper);
        return  R.ok().data("item",banneres);
    }
}
