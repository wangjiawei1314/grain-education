package com.atguigu.eduCms.mapper;

import com.atguigu.eduCms.entity.CrmBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 首页banner表 Mapper 接口
 * </p>
 *
 * @author wjw
 * @since 2020-10-29
 */
public interface CrmBannerMapper extends BaseMapper<CrmBanner> {

}
