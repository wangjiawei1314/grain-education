package com.atguigu.eduCms.controller;


import com.atguigu.commonutils.R;
import com.atguigu.eduCms.entity.CrmBanner;
import com.atguigu.eduCms.service.CrmBannerService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 首页banner表 前端控制器
 * </p>
 *
 * @author wjw
 * @since 2020-10-29
 */
@Api(description ="轮播图管理")
@RestController
@RequestMapping("/eduCms/banner-admin")
//@CrossOrigin
public class BannerAdminController {

    @Autowired
    private CrmBannerService crmBannerService;

    /**
     * 分页查询轮播图
     */
    @GetMapping("pageBanner/{page}/{limit}")
    public R pageBanner(
            @ApiParam(value = "页码",name = "page",required = true)
            @PathVariable Long page,
            @ApiParam(value = "条数",name = "limit",required = true)
            @PathVariable Long limit
    ){
        Page<CrmBanner> pageParam = new Page<>(page,limit);
        crmBannerService.pageMaps(pageParam,null);
        List<CrmBanner> crmBanners = pageParam.getRecords();
        Long totals = pageParam.getTotal();
        return R.ok().data("item",crmBanners).data("total",totals);
    }

    /**
     * 添加轮播图
     */
    @PostMapping("addBanner")
    public R addBanner(
            @ApiParam(value = "幻灯片",name = "banner",required = true)
            @RequestBody CrmBanner crmBanner){
        crmBannerService.save(crmBanner);
        return R.ok();
    }

    /**
     *修改轮播图
     */
    @PostMapping("updateBanner")
    public R updateBanner(
            @ApiParam(value = "幻灯片",name = "banner",required = true)
            @RequestBody CrmBanner crmBanner
    ){
        crmBannerService.updateById(crmBanner);
        return R.ok();
    }

    /**
     * 删除轮播图
     */
    @DeleteMapping("deleteBanner")
    public R deleteBanner(
            @ApiParam(value = "幻灯片Id",name = "id",required = true)
            @PathVariable String id
    ){
        crmBannerService.removeById(id);
        return R.ok();
    }
}

