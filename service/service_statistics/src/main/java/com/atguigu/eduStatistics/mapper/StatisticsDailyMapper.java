package com.atguigu.eduStatistics.mapper;

import com.atguigu.eduStatistics.entity.StatisticsDaily;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 网站统计日数据 Mapper 接口
 * </p>
 *
 * @author wjw
 * @since 2020-11-10
 */
public interface StatisticsDailyMapper extends BaseMapper<StatisticsDaily> {

}
