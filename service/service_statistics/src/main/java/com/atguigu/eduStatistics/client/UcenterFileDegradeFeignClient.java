package com.atguigu.eduStatistics.client;

import com.atguigu.commonutils.R;
import org.springframework.stereotype.Component;

@Component
public class UcenterFileDegradeFeignClient implements UcenterClient {

    @Override
    public R registerCount(String day) {
            return R.error().message("------获取注册人数失败-------");
    }
}
