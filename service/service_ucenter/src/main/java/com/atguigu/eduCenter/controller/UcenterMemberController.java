package com.atguigu.eduCenter.controller;


import com.atguigu.commonutils.JwtUtils;
import com.atguigu.commonutils.orderVo.MemberOrderVo;
import com.atguigu.commonutils.R;
import com.atguigu.eduCenter.entity.UcenterMember;
import com.atguigu.eduCenter.entity.vo.RegistUserVo;
import com.atguigu.eduCenter.service.UcenterMemberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 会员表 前端控制器
 * </p>
 *
 * @author wjw
 * @since 2020-10-31
 */
@Api("登录")
@RestController
@RequestMapping("/eduCenter/member")
//@CrossOrigin
public class UcenterMemberController {

    @Autowired
    private UcenterMemberService ucenterMemberService;

    @ApiOperation("登录用户的校验")
    @PostMapping("uLogin")
    public R uLogin(@RequestBody UcenterMember ucenterMember){
        String  token = ucenterMemberService.login(ucenterMember);
        return R.ok().data("token",token);
    }

    //注册用户
    @ApiOperation("注册")
    @PostMapping("RegisterUser")
    public R RegisterUser(@RequestBody RegistUserVo registUserVo){
        ucenterMemberService.register(registUserVo);
        return R.ok().message("注册成功");
    }

    //获取token值
    @ApiOperation(value = "根据token获取登录信息")
    @GetMapping("auth/getLoginInfo")
    public R getLoginInfo(HttpServletRequest request){
        String userId = JwtUtils.getMemberIdByJwtToken(request);
        UcenterMember user =  ucenterMemberService.getById(userId);
        return R.ok().data("user",user);
    }

    //根据token字符串获取用户信息
    @PostMapping("getInfoUc/{id}")
    public MemberOrderVo getInfoUc(@PathVariable String id){
        UcenterMember user = ucenterMemberService.getById(id);
        MemberOrderVo memberOrderVo = new MemberOrderVo();
        BeanUtils.copyProperties(user,memberOrderVo);
        return memberOrderVo;
    }

    //根据日期统计注册人数
    @GetMapping(value = "countRegister/{day}")
    public R registerCount(
            @PathVariable String day){
        Integer count = ucenterMemberService.countRegisterByDay(day);
        return R.ok().data("countRegister", count);
    }
}


