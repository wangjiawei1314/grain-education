package com.atguigu.eduCenter.service.impl;

import com.atguigu.commonutils.JwtUtils;
import com.atguigu.commonutils.MD5;
import com.atguigu.eduCenter.entity.UcenterMember;
import com.atguigu.eduCenter.entity.vo.RegistUserVo;
import com.atguigu.eduCenter.mapper.UcenterMemberMapper;
import com.atguigu.eduCenter.service.UcenterMemberService;
import com.atguigu.servicebase.exceptionHandler.GuliException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author wjw
 * @since 2020-10-31
 */
@Service
public class UcenterMemberServiceImpl extends ServiceImpl<UcenterMemberMapper, UcenterMember> implements UcenterMemberService {

    @Autowired
    private RedisTemplate redisTemplate;


    @Override
    public String login(UcenterMember ucenterMember) {
        String phone = ucenterMember.getMobile();
        String passWord = ucenterMember.getPassword();

        if(StringUtils.isEmpty(phone)||StringUtils.isEmpty(passWord)){
            throw new GuliException(20001,"登录用户名或密码不能为空");
        }
        QueryWrapper<UcenterMember>  queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("mobile",phone);
        UcenterMember user = baseMapper.selectOne(queryWrapper);

        if(null == user){
            throw new GuliException(20001,"登录用户不存在!");
        }

        if(!MD5.encrypt(passWord).equals(user.getPassword())){
            throw new GuliException(20001,"密码不正确!");
        }

        //校验是否被禁用
        if(user.getIsDisabled()) {
            throw new GuliException(20001,"该用户被禁用");
        }

        String token = JwtUtils.getJwtToken(user.getId(),user.getNickname());
        return token;
    }

    @Override
    public void register(RegistUserVo registUserVo) {
        String  mobile = registUserVo.getMobile();
        String  nickname = registUserVo.getNickname();
        String  password = registUserVo.getPassword();
        String  code = registUserVo.getCode();

        if(StringUtils.isEmpty(mobile)||StringUtils.isEmpty(password)
        ||StringUtils.isEmpty(nickname)||StringUtils.isEmpty(code)){
            throw new GuliException(20001,"注册失败");
        }

        //校验校验验证码
        //从redis获取发送的验证码
        String mobleCode = redisTemplate.opsForValue().get(mobile).toString();
        if(!code.equals(mobleCode)) {
            throw new GuliException(20001,"error");
        }

        //查询数据库中是否存在相同的手机号码
        Integer count = baseMapper.selectCount(new QueryWrapper<UcenterMember>().eq("mobile", mobile));
        if(count.intValue() > 0) {
            throw new GuliException(20001,"error");
        }

        //添加注册信息到数据库
        UcenterMember member = new UcenterMember();
        member.setNickname(nickname);
        member.setMobile(mobile);
        member.setPassword(MD5.encrypt(password));
        member.setIsDisabled(false);
        member.setAvatar("http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132");
        this.save(member);
    }

    @Override
    public UcenterMember getByOpenid(String openid) {
        QueryWrapper  queryWrapper = new QueryWrapper();
        queryWrapper.eq("openid",openid);
        UcenterMember member = baseMapper.selectOne(queryWrapper);
        return member;
    }

    @Override
    public Integer countRegisterByDay(String day) {
        return baseMapper.selectRegisterCount(day);
    }
}
