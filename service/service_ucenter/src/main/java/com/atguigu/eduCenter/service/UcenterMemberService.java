package com.atguigu.eduCenter.service;

import com.atguigu.eduCenter.entity.UcenterMember;
import com.atguigu.eduCenter.entity.vo.RegistUserVo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author wjw
 * @since 2020-10-31
 */
public interface UcenterMemberService extends IService<UcenterMember> {

    String login(UcenterMember ucenterMember);

    void register(RegistUserVo registUserVo);

    UcenterMember getByOpenid(String openid);

    Integer countRegisterByDay(String day);
}
