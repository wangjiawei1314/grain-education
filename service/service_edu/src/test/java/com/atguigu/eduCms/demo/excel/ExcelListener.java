package com.atguigu.eduCms.demo.excel;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import java.util.Map;

public class ExcelListener  extends AnalysisEventListener<readData> {

    //一行一行去读取Excel的内容
    @Override
    public void invoke(readData readData, AnalysisContext analysisContext) {
        System.out.println("***"+readData);
    }
    //读取excel表头信息
    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        System.out.println("表头信息："+headMap);
    }
    //读取完后执行
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
    }
}
