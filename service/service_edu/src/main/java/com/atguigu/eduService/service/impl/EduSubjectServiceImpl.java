package com.atguigu.eduService.service.impl;

import com.alibaba.excel.EasyExcel;
import com.atguigu.eduService.Listener.SubjectExcelListen;
import com.atguigu.eduService.entity.EduSubject;
import com.atguigu.eduService.entity.OneSubject;
import com.atguigu.eduService.entity.TwoSubject;
import com.atguigu.eduService.entity.excel.ExcelSubjectData;
import com.atguigu.eduService.mapper.EduSubjectMapper;
import com.atguigu.eduService.service.EduSubjectService;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程科目 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-08-23
 */
@Service
public class EduSubjectServiceImpl extends ServiceImpl<EduSubjectMapper, EduSubject> implements EduSubjectService {

    @Override
    public void importSubjectData(MultipartFile file, EduSubjectService eduSubjectService) {
        try {
            InputStream in = file.getInputStream();

            EasyExcel.read(in, ExcelSubjectData.class,new SubjectExcelListen(eduSubjectService)).sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<OneSubject> getAllOneTwoSubject() {
        //查询一级分类
        QueryWrapper<EduSubject> oneWapper = new QueryWrapper<EduSubject>();
        oneWapper.eq("parent_id","0");
        List<EduSubject>  oSubject = baseMapper.selectList(oneWapper);
        //查询二级分类
        QueryWrapper<EduSubject> twoWapper = new QueryWrapper<EduSubject>();
        oneWapper.ne("parent_id","0");
        List<EduSubject>  tSubject = baseMapper.selectList(twoWapper);

        List<OneSubject> finalAllSubject = new ArrayList<>();
        //将一级分类写入总的分类中
        for (int i = 0; i < oSubject.size(); i++) {
                EduSubject eSubject = oSubject.get(i);
                OneSubject oneSubject = new OneSubject();
                BeanUtils.copyProperties(eSubject,oneSubject);

             List<TwoSubject> finalTwoSubject = new ArrayList<>();
                for (int j = 0; j < tSubject.size() ; j++) {
                    EduSubject fSubject = tSubject.get(j);
                    if(fSubject.getParentId().equals(oneSubject.getId())){
                        TwoSubject twoSubject = new TwoSubject();
                        BeanUtils.copyProperties(fSubject,twoSubject);
                        finalTwoSubject.add(twoSubject);
                    }
                }
            oneSubject.setChildren(finalTwoSubject);
            finalAllSubject.add(oneSubject);
        }
        return finalAllSubject;
    }
}
