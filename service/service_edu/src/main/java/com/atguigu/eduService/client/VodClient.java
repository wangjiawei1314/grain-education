package com.atguigu.eduService.client;


import com.atguigu.commonutils.R;
import io.swagger.annotations.ApiParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient("service-vod")
@Component
public interface VodClient {

    @DeleteMapping("/eduVod/video/removeVodById/{vodId}")
    R removeVodById(
            @ApiParam(name = "id", value = "视频Id", required = true)
            @PathVariable("vodId") String vodId
    );

    @DeleteMapping("/eduVod/video/removeVodListByIds")
    R removeVodListByIds(
            @ApiParam(name = "id", value = "视频Id", required = true)
            @RequestParam("vodList") List<String> vodList
    );
}
