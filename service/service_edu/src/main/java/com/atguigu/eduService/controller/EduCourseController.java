package com.atguigu.eduService.controller;

import com.atguigu.commonutils.R;
import com.atguigu.commonutils.orderVo.CourseOrderVo;
import com.atguigu.eduService.entity.EduCourse;
import com.atguigu.eduService.entity.vo.courseInfoVo;
import com.atguigu.eduService.entity.vo.coursePublishInfo;
import com.atguigu.eduService.entity.vo.courseQuery;
import com.atguigu.eduService.service.EduCourseService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(description ="课程管理")
@RestController
@RequestMapping("/eduService/edu-course")
//@CrossOrigin
public class EduCourseController  {

    @Autowired
    private EduCourseService eduCourseService;

    @ApiOperation("查询课程列表")
    @PostMapping("getCourseList")
    public R getCourseList(){
        List<EduCourse> list = eduCourseService.list(null);
        return R.ok().data("item",list);
    }
    //逻辑删除讲师
    @ApiOperation(value="删除课程以及关联的章节小节信息")
    @DeleteMapping("{courseId}")
    public R removeCourse(@ApiParam(name = "id",value = "课程Id",required = true) @PathVariable String courseId){
        eduCourseService.removeCourse(courseId);
        return R.ok();
    }
    //分页查询课程信息
    @ApiOperation(value="讲师分页查询")
    @GetMapping("pageListCourse/{current}/{limit}")
    public R pageListTeacher(@PathVariable Long current,@PathVariable Long limit){

        Page<EduCourse> pageParam = new Page<>(current, limit);

        eduCourseService.pageMaps(pageParam,null);
        List<EduCourse> records =  pageParam.getRecords();
        Long totals = pageParam.getTotal();
        return R.ok().data("total",totals).data("rows",records);
    }

    //条件查询所有讲师的信息
    @ApiOperation(value="讲师条件分页查询")
    @PostMapping("pageCourseCondition/{current}/{limit}")
    public R pageCourseCondition(@PathVariable Long current ,@PathVariable Long limit ,
                                  @RequestBody(required = false) courseQuery courseQuery){

        Page<EduCourse> pageParam = new Page<EduCourse>(current,limit);
        QueryWrapper<EduCourse> wapper = new QueryWrapper<EduCourse>();
        String title   = courseQuery.getTitle();
        String subject = courseQuery.getSubject();
        String status  = courseQuery.getStatus();
        String  time   = courseQuery.getTime();
        if(!StringUtils.isEmpty(title)){
            wapper.like("title",title);
        }
        if(!StringUtils.isEmpty(time)){
            wapper.ge("gmt_create",time);//大于等于
        }
        if(!StringUtils.isEmpty(status)){
            wapper.eq("status",status);
        }
        if(!StringUtils.isEmpty(subject)){
            wapper.eq("subject_parent_id",subject);
        }

        wapper.orderByDesc("gmt_create");
        eduCourseService.page(pageParam,wapper);
        List<EduCourse> records =  pageParam.getRecords();
        Long totals = pageParam.getTotal();
        return R.ok().data("total",totals).data("rows",records);

    }

    @ApiOperation("添加课程")
    @PostMapping("addCourseInfo")
    public R addCourseInfo(@RequestBody courseInfoVo courseInfoVo) {
        String str = eduCourseService.saveCourseInfo(courseInfoVo);
        return R.ok().data("courseId",str);
    }


    //根据课程Id  回显课程信息
    @ApiOperation("回显课程")
    @GetMapping("getCourseInfo/{courseId}")
    public R getCourseInfo(@PathVariable String courseId) {
        courseInfoVo courseInfo = eduCourseService.getCourseInfo(courseId);
        return R.ok().data("courseInfo",courseInfo);
    }

    //修改课程信息
    @ApiOperation("修改课程")
    @PostMapping("updateCourseInfo")
    public R updateCourseInfo(@RequestBody courseInfoVo courseInfoVo){
        eduCourseService.updateCourseInfo(courseInfoVo);
        return R.ok();
    }

    //最终发布课程的信息
    @ApiOperation("最终发布课程")
    @GetMapping("getCoursePublish/{courseId}")
    public R getCoursePublishByCourseId(@PathVariable String courseId){
        coursePublishInfo coursePublish =  eduCourseService.getCoursePublishByCourseId(courseId);
        return R.ok().data("coursePublish",coursePublish);
    }

    //发布课程信息
    @ApiOperation("发布课程")
    @PostMapping("coursePublish/{courseId}")
    public R coursePublish(@PathVariable String courseId){
        EduCourse eduCourse = new EduCourse();
        eduCourse.setId(courseId);
        eduCourse.setStatus("Normal");
        eduCourseService.updateById(eduCourse);
        return R.ok();
    }

    //获取课程信息
    @GetMapping("getCourseOrderInfo/{courseId}")
    public CourseOrderVo getCourseOrderInfo(@PathVariable String courseId){
       courseInfoVo courseInfoVo = eduCourseService.getCourseInfo(courseId);
       CourseOrderVo courseOrderVo = new CourseOrderVo();
       BeanUtils.copyProperties(courseInfoVo,courseOrderVo);
       return courseOrderVo;
    }

}
