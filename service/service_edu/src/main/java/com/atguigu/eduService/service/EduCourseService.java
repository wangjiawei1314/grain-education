package com.atguigu.eduService.service;

import com.atguigu.eduService.entity.EduCourse;
import com.atguigu.eduService.entity.frontvo.CourseWebVo;
import com.atguigu.eduService.entity.frontvo.courseFrontVo;
import com.atguigu.eduService.entity.vo.courseInfoVo;
import com.atguigu.eduService.entity.vo.coursePublishInfo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author testjava
 * @since 2020-09-21
 */
public interface EduCourseService extends IService<EduCourse> {

    String saveCourseInfo(courseInfoVo courseInfoVo);

    courseInfoVo getCourseInfo(String courseId);

    void updateCourseInfo(courseInfoVo courseInfoVo);

    coursePublishInfo getCoursePublishByCourseId(String courseId);

    void removeCourse(String courseId);

    List<EduCourse> selectByTeacherId(String teacherId);

    Map<String, Object> pageListWeb(Page<EduCourse> pageParam, courseFrontVo courseFrontVo);

    CourseWebVo queryCourseInfo(String courseId);
}
