package com.atguigu.eduService.controller.front;

import com.atguigu.commonutils.R;
import com.atguigu.eduService.entity.EduCourse;
import com.atguigu.eduService.entity.EduTeacher;
import com.atguigu.eduService.service.EduCourseService;
import com.atguigu.eduService.service.EduTeacherService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Api(description = "前端讲师查询")
@RestController
@RequestMapping("/eduService/front-teacher")
//@CrossOrigin
public class TeacherFrontController {
    @Autowired
    private EduTeacherService eduTeacherService;
    @Autowired
    private EduCourseService courseService;

    @ApiOperation(value = "分页查询讲师")
    @PostMapping("QueryTeacherPage/{current}/{limit}")
    public R QueryAllTeacher(@PathVariable Long current,@PathVariable Long limit){
        Page<EduTeacher> page = new Page<>(current,limit);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("id");
        Map map =eduTeacherService.getTeacherPage(page,queryWrapper);
        return R.ok().data(map);
    }

    @GetMapping("getTeacherFrontInfo/{teacherId}")
    public R getTeacherFrontInfo(@PathVariable String teacherId){
        //1.查询讲师的详情信息
        EduTeacher teacher = eduTeacherService.getById(teacherId);
        //2.查询讲师所讲的课程列表
        List<EduCourse> courseList = courseService.selectByTeacherId(teacherId);

        return R.ok().data("teacher",teacher).data("courseList",courseList);
    }
}
