package com.atguigu.eduService.controller;


import com.atguigu.commonutils.R;
import com.atguigu.eduService.entity.EduVideo;
import com.atguigu.eduService.service.EduVideoService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 课程视频 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2020-09-21
 */
@RestController
@RequestMapping("/eduService/edu-video")
//@CrossOrigin
public class EduVideoController {

    @Autowired
    private EduVideoService eduVideoService;

    @ApiOperation(value = "根据Id ")
    @PostMapping("addVideo")
    public R addVideo(
            @ApiParam(value = "EduVideo",name = "添加小节信息",required = true)
            @RequestBody EduVideo eduVideo
            ){
         eduVideoService.save(eduVideo);
         return R.ok();
    }

    @ApiOperation(value = "删除Id ")
    @DeleteMapping("removeVideoById/{videoId}")
    public R deleteVide(
            @ApiParam(value = "id",name = "删除小节信息",required = true)
            @PathVariable String  videoId
    ){
        boolean result = eduVideoService.deleteById(videoId);
        if (result){
            return  R.ok();
        }else{
            return R.error().message("删除失败");
        }
    }

    @ApiOperation(value = "根据Id查找小节 ")
    @GetMapping("getVideoById/{videoId}")
    public R getVideoById(
            @ApiParam(value = "id",name = "根据Id查找小节",required = true)
            @PathVariable String  videoId
    ){
        EduVideo result = eduVideoService.getById(videoId);
            return  R.ok().data("item",result);
    }


    @ApiOperation(value = "修改小节信息")
    @PostMapping("updateVideo")
    public R updateVideo(
            @ApiParam(value = "eduVideo",name = "更新小节信息",required = true)
            @RequestBody EduVideo eduVideo
    ){
        eduVideoService.updateById(eduVideo);
        return R.ok();
    }
}

