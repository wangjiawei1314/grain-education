package com.atguigu.eduService.entity.vo;

import lombok.Data;

@Data
public class coursePublishInfo {

    private String id;

    private String cover;

    private String title;

    private String price;

    private Integer lessonNum;

    private String  teacherName;

    private String subjectLevelOne;

    private String subjectLevelTwo;

    private String description;
}
