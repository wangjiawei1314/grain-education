package com.atguigu.eduService.service.impl;

import com.atguigu.eduService.entity.EduChapter;
import com.atguigu.eduService.entity.EduVideo;
import com.atguigu.eduService.entity.chapter.chapterVo;
import com.atguigu.eduService.entity.chapter.videoVo;
import com.atguigu.eduService.mapper.EduChapterMapper;
import com.atguigu.eduService.service.EduChapterService;
import com.atguigu.eduService.service.EduVideoService;
import com.atguigu.servicebase.exceptionHandler.GuliException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-09-21
 */
@Service
public class EduChapterServiceImpl extends ServiceImpl<EduChapterMapper, EduChapter> implements EduChapterService {

    @Autowired
    EduVideoService eduVideoService;

    @Override
    public List<chapterVo> getChapterVideoByCourseId(String courseId) {
        //1.通过课程Id 获取章列表
        QueryWrapper<EduChapter> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("course_id", courseId);
        queryWrapper1.orderByAsc("sort", "id");
        List<EduChapter> eduChapter = baseMapper.selectList(queryWrapper1);
        //2.通过课程Id 获取节列表
        QueryWrapper<EduVideo> queryWrapper2 = new QueryWrapper<>();
        queryWrapper2.eq("course_id", courseId);
        queryWrapper2.orderByAsc("sort", "id");
        List<EduVideo> eduVideo = eduVideoService.list(queryWrapper2);

        List<chapterVo> chapterVoList = new ArrayList<>();
        for(int i = 0;i < eduChapter.size();i++){
            chapterVo  chapterVo= new chapterVo();
            EduChapter eduChapter1 = eduChapter.get(i);
            BeanUtils.copyProperties(eduChapter1,chapterVo);

            chapterVoList.add(chapterVo);
            List<videoVo> videoVos = new ArrayList<>();
            for(int j = 0;j < eduVideo.size();j++){
                EduVideo eduVideo1 = eduVideo.get(j);
                videoVo videoVo = new videoVo();
                if(eduVideo1.getChapterId().equals(eduChapter1.getId())){
                    BeanUtils.copyProperties(eduVideo1,videoVo);
                    videoVos.add(videoVo);
                }
            }
            chapterVo.setChildren(videoVos);
        }
        return chapterVoList;
    }

    @Override
    public boolean removeChapterById(String id) {
        //根据Id查询是否存在视频，如果有则提示用户上有子节点
        if(eduVideoService.getCountByChapterId(id)){
            throw new GuliException(20001,"该分章节下存在视频课程，请先删除视频课程");
        }
        Integer result = baseMapper.deleteById(id);
        return null != result && result > 0;
    }
}
