package com.atguigu.eduService.service.impl;

import com.atguigu.commonutils.R;
import com.atguigu.eduService.client.VodClient;
import com.atguigu.eduService.entity.EduCourse;
import com.atguigu.eduService.entity.EduCourseDescription;
import com.atguigu.eduService.entity.EduVideo;
import com.atguigu.eduService.entity.frontvo.CourseWebVo;
import com.atguigu.eduService.entity.frontvo.courseFrontVo;
import com.atguigu.eduService.entity.vo.courseInfoVo;
import com.atguigu.eduService.entity.vo.coursePublishInfo;
import com.atguigu.eduService.mapper.EduCourseMapper;
import com.atguigu.eduService.service.EduChapterService;
import com.atguigu.eduService.service.EduCourseDescriptionService;
import com.atguigu.eduService.service.EduCourseService;
import com.atguigu.eduService.service.EduVideoService;
import com.atguigu.servicebase.exceptionHandler.GuliException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-09-21
 */
@Service
public class EduCourseServiceImpl extends ServiceImpl<EduCourseMapper, EduCourse> implements EduCourseService {

    @Autowired
    private EduCourseDescriptionService eduCourseDescriptionService;

    @Autowired
    private EduVideoService eduVideoService;

    @Autowired
    private EduChapterService eduChapterService;

    @Autowired
    private VodClient vodClient;

    @Override
    public String saveCourseInfo(courseInfoVo courseInfoVo){
        EduCourse  eduCourse = new EduCourse();
        BeanUtils.copyProperties(courseInfoVo,eduCourse);
        int  i = baseMapper.insert(eduCourse);
        if(i == 0){
             throw new GuliException(20001,"添加课程信息失败");
        }

        //获取添加之后课程id
        String cId = eduCourse.getId();
        //2 向课程简介表添加课程简介
        EduCourseDescription courseDescription = new EduCourseDescription();
        courseDescription.setDescription(courseInfoVo.getDescription());
        courseDescription.setId(cId);
        eduCourseDescriptionService.save(courseDescription);

        return cId;
    }

    @Override
    public courseInfoVo getCourseInfo(String courseId) {

        //根据课程ID 首先获取课程信息
        courseInfoVo courseInfo  = new courseInfoVo();
        EduCourse eduCourse = baseMapper.selectById(courseId);
        BeanUtils.copyProperties(eduCourse,courseInfo);

        EduCourseDescription eduCourseDescription = eduCourseDescriptionService.getById(courseId);
        courseInfo.setDescription(eduCourseDescription.getDescription());

        return courseInfo;
    }

    @Override
    public void updateCourseInfo(courseInfoVo courseInfoVo) {
        EduCourse eduCourse = new EduCourse();
        BeanUtils.copyProperties(courseInfoVo,eduCourse);
        int insert = baseMapper.updateById(eduCourse);
        if(insert == 0 ){
            throw new GuliException(20001,"修改课程信息失败");
        }

        EduCourseDescription description = new EduCourseDescription();
        description.setId(courseInfoVo.getId());
        description.setDescription(courseInfoVo.getDescription());
        eduCourseDescriptionService.updateById(description);

    }

    @Override
    public coursePublishInfo getCoursePublishByCourseId(String courseId) {
        return  baseMapper.getCoursePublishByCourseId(courseId);

    }

    @Override
    public void removeCourse(String courseId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("course_id",courseId);
        queryWrapper.select("video_source_id");
        List<EduVideo>  eduList = eduVideoService.list(queryWrapper);
        List<String>  str = new ArrayList<>() ;
        for(EduVideo eduVideo:eduList){
           String vodSourseId =  eduVideo.getVideoSourceId();
           if(!vodSourseId.isEmpty()){
               str.add(vodSourseId);
           }
        }

        if(str.size() > 0){
          R result  =    vodClient.removeVodListByIds(str);
            if(result.getCode() == 20001){
                throw new GuliException(20001,"---熔断器已断开---");
            }
        }

        Map map =new HashMap<String,String>();
        map.put("course_id",courseId);
         eduVideoService.removeByMap(map);

         eduChapterService.removeByMap(map);

         eduCourseDescriptionService.removeById(courseId);

        int result = baseMapper.deleteById(courseId);
        if(result == 0){
            throw new GuliException(20001,"删除异常");
        }
    }

    @Override
    public List<EduCourse> selectByTeacherId(String teacherId) {
        QueryWrapper<EduCourse> queryWrapper = new QueryWrapper();
        queryWrapper.eq("teacher_id",teacherId);
        queryWrapper.orderByDesc("gmt_create");
        List<EduCourse> list = baseMapper.selectList(queryWrapper);
        return list;
    }

    @Override
    public Map<String, Object> pageListWeb(Page<EduCourse> pageParam, courseFrontVo courseFrontVo) {
        QueryWrapper<EduCourse> wapper = new QueryWrapper<>();
        if(!StringUtils.isEmpty(courseFrontVo.getSubjectParentId())){
            wapper.eq("subject_parent_id",courseFrontVo.getSubjectParentId());
        }
        if (!StringUtils.isEmpty(courseFrontVo.getSubjectId())){
            wapper.eq("subject_id",courseFrontVo.getSubjectId());
        }
        if (!StringUtils.isEmpty(courseFrontVo.getGmtCreateSort())){
            wapper.eq("gmt_create",courseFrontVo.getGmtCreateSort());
        }
        if (!StringUtils.isEmpty(courseFrontVo.getPriceSort())){
            wapper.eq("price",courseFrontVo.getPriceSort());
        }
        if(!StringUtils.isEmpty(courseFrontVo.getBuyCountSort())){
            wapper.eq("buy_count",courseFrontVo.getBuyCountSort());
        }
        baseMapper.selectPage(pageParam,wapper);
        List<EduCourse>  records = pageParam.getRecords();
        long current = pageParam.getCurrent();
        long pages = pageParam.getPages();
        long size = pageParam.getSize();
        long total = pageParam.getTotal();
        boolean hasNext = pageParam.hasNext();
        boolean hasPrevious = pageParam.hasPrevious();

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("items", records);
        map.put("current", current);
        map.put("pages", pages);
        map.put("size", size);
        map.put("total", total);
        map.put("hasNext", hasNext);
        map.put("hasPrevious", hasPrevious);

        return map;
    }

    @Override
    public CourseWebVo queryCourseInfo(String courseId) {
        return baseMapper.queryCourseInfo(courseId);
    }
}
