package com.atguigu.eduService.mapper;

import com.atguigu.eduService.entity.EduCourse;
import com.atguigu.eduService.entity.frontvo.CourseWebVo;
import com.atguigu.eduService.entity.vo.coursePublishInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.data.repository.query.Param;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-09-21
 */
public interface EduCourseMapper extends BaseMapper<EduCourse> {

    coursePublishInfo getCoursePublishByCourseId(@Param("courseId") String courseId);

    CourseWebVo queryCourseInfo(String courseId);
}
