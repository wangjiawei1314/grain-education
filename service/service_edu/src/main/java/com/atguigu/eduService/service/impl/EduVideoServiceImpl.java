package com.atguigu.eduService.service.impl;

import com.atguigu.commonutils.R;
import com.atguigu.eduService.client.VodClient;
import com.atguigu.eduService.entity.EduVideo;
import com.atguigu.eduService.mapper.EduVideoMapper;
import com.atguigu.eduService.service.EduVideoService;
import com.atguigu.servicebase.exceptionHandler.GuliException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.sql.SqlHelper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程视频 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-09-21
 */
@Service
public class EduVideoServiceImpl extends ServiceImpl<EduVideoMapper, EduVideo> implements EduVideoService {

    @Autowired
    private VodClient vodClient;
    @Override
    public boolean getCountByChapterId(String id) {
        QueryWrapper<EduVideo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("chapter_id", id);
        Integer count = baseMapper.selectCount(queryWrapper);
        return null != count && count > 0;
    }

    @Override
    public boolean deleteById(String videoId) {
        EduVideo eduVideo = baseMapper.selectById(videoId);
        String videoSourseId = eduVideo.getVideoSourceId();
        if(!videoSourseId.isEmpty()){
           R  result =   vodClient.removeVodById(videoSourseId);
           if(result.getCode() == 20001){
               throw new GuliException(20001,"---熔断器已断开---");
           }
        }
        return SqlHelper.delBool(baseMapper.deleteById(videoId));
    }
}
