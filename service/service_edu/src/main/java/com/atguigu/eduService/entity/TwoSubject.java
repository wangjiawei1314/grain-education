package com.atguigu.eduService.entity;

import lombok.Data;

@Data
public class TwoSubject {

    private String id;
    private String title;
}
