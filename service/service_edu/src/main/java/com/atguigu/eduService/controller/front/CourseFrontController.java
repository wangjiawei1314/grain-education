package com.atguigu.eduService.controller.front;

import com.atguigu.commonutils.JwtUtils;
import com.atguigu.commonutils.R;
import com.atguigu.eduService.client.OrderClient;
import com.atguigu.eduService.entity.EduCourse;
import com.atguigu.eduService.entity.chapter.chapterVo;
import com.atguigu.eduService.entity.frontvo.CourseWebVo;
import com.atguigu.eduService.entity.frontvo.courseFrontVo;
import com.atguigu.eduService.service.EduChapterService;
import com.atguigu.eduService.service.EduCourseService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Api(description = "前端讲师查询")
@RestController
@RequestMapping("/eduService/front-course")
//@CrossOrigin
public class CourseFrontController {
    @Autowired
    private EduCourseService courseService;
    @Autowired
    private EduChapterService eduChapterService;
    @Autowired
    private OrderClient orderClient;
    //分页查询课程列表
    @PostMapping("QueryCoursePage/{page}/{limit}")
    public R QueryCoursePage(@PathVariable Long page,@PathVariable Long limit,
                             @RequestBody(required = false) courseFrontVo courseFrontVo){
        Page<EduCourse> pageParam = new Page<>(page, limit);
        Map<String, Object> map = courseService.pageListWeb(pageParam, courseFrontVo);
        return  R.ok().data(map);
    }

    //查询课程详情信息
    @GetMapping("QueryCourseInfo/{courseId}")
    public R QueryCourseInfo(@PathVariable String courseId, HttpServletRequest request){
        CourseWebVo courseWebVo = courseService.queryCourseInfo(courseId);
        List<chapterVo>  chapterList = eduChapterService.getChapterVideoByCourseId(courseId);
        //根据课程Id和用户Id查询是否已支付过该课程
        String member = JwtUtils.getMemberIdByJwtToken(request);
        boolean isBuy;
        if(member == null || member ==""){
            isBuy =  false;
        }else{
           isBuy = orderClient.isBuyCourse(courseId,member);
        }

        return R.ok().data("courseWebVo",courseWebVo).data("chapterList",chapterList).data("isBuy",isBuy);
    }

}
