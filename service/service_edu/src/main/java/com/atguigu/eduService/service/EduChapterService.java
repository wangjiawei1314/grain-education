package com.atguigu.eduService.service;

import com.atguigu.eduService.entity.EduChapter;
import com.atguigu.eduService.entity.chapter.chapterVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author testjava
 * @since 2020-09-21
 */
public interface EduChapterService extends IService<EduChapter> {

    List<chapterVo> getChapterVideoByCourseId(String courseId);

    boolean removeChapterById(String id);
}
