package com.atguigu.eduService.entity.chapter;

import lombok.Data;

@Data
public class videoVo {

    private String id;

    private String title;

    private String videoSourceId;
}
