package com.atguigu.eduService.service;

import com.atguigu.eduService.entity.EduTeacher;
import com.atguigu.eduService.entity.vo.teacherQuery;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 讲师 服务类
 * </p>
 *
 * @author testjava
 * @since 2020-07-21
 */
public interface EduTeacherService extends IService<EduTeacher> {
    void pageQuery(Page<EduTeacher> pageParam, teacherQuery teacherQuery);

    Map getTeacherPage(Page<EduTeacher> page, QueryWrapper o);
}
