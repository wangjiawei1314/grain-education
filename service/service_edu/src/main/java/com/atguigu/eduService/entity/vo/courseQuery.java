package com.atguigu.eduService.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class courseQuery {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "课程名称，模糊查询")
    private String title;

    @ApiModelProperty(value = "课程分类")
    private String subject;

    @ApiModelProperty(value = "课程的发布状态")
    private String status;

    @ApiModelProperty(value = "课程的创建时间" , example = "2019-12-01 10:10:10")
    private String time;


}
