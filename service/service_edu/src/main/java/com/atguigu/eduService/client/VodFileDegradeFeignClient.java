package com.atguigu.eduService.client;

import com.atguigu.commonutils.R;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class VodFileDegradeFeignClient implements VodClient {
    @Override
    public R removeVodById(String vodId) {
        return R.error().message("------删除视频失败了-------");
    }

    @Override
    public R removeVodListByIds(List<String> vodList) {
        return R.error().message("------删除多个视频失败了-------");
    }
}
