package com.atguigu.eduService.controller;


import com.atguigu.commonutils.R;
import com.atguigu.eduService.entity.EduChapter;
import com.atguigu.eduService.entity.chapter.chapterVo;
import com.atguigu.eduService.service.EduChapterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2020-09-21
 */
@Api(description ="章节管理")
@RestController
@RequestMapping("/eduService/edu-chapter")
//@CrossOrigin
public class EduChapterController {

    @Autowired
    EduChapterService eduChapterService;

    @GetMapping("getChapterVideoByCourseId/{courseId}")
    public R getChapterVideoByCourseId(@PathVariable String courseId){
       List<chapterVo> allChapterVideo = eduChapterService.getChapterVideoByCourseId(courseId);
       return R.ok().data("allChapterVideo",allChapterVideo);
    }

    @PostMapping("addChapterInfo")
    public R  addChapterInfo(@RequestBody EduChapter eduChapter){
        eduChapterService.save(eduChapter);
        return R.ok();
    }

    //根据Id查询章节
    @ApiOperation(value = "根据ID查询章节")
    @GetMapping("getChapterInfo/{id}")
    public R  getChapterById(
            @ApiParam(name = "id" ,value = "章节Id",required = true)
            @PathVariable String id){
        EduChapter eduChapter = eduChapterService.getById(id);
        return R.ok().data("item", eduChapter);
    }

    //根据Id查找章节
    @ApiOperation(value = "根据Id修改章节")
    @PostMapping("updateChapter")
    public  R updateChapter(
            @ApiParam(name = "chapter", value = "章节对象", required = true)
            @RequestBody EduChapter chapter){
            eduChapterService.updateById(chapter);
            return R.ok();
    }

    //根据Id删除章节
    @DeleteMapping("removeById/{chapterId}")
    public R removeById(
            @ApiParam(name = "id",value = "根据Id删除章节",required = true)
            @PathVariable String chapterId
    ){
        boolean result = eduChapterService.removeChapterById(chapterId);
        if (result){
            return  R.ok();
        }else{
            return R.error().message("删除失败");
        }
    }


}

