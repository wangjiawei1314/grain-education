package com.atguigu.eduService.controller;


import com.atguigu.commonutils.R;
import com.atguigu.eduService.entity.EduTeacher;
import com.atguigu.eduService.entity.vo.teacherQuery;
import com.atguigu.eduService.service.EduTeacherService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2020-07-21
 */
@Api(description ="讲师管理")
@RestController
@RequestMapping("/eduService/edu-teacher")
//@CrossOrigin
public class EduTeacherController {

    //把Service注入
    @Autowired
    private EduTeacherService eduTeacherService;
    //查询所有讲师的信息
    @ApiOperation(value="所有讲师信息")
    @GetMapping("findAll")
    public R findAlllteacher(){
        List<EduTeacher> list = eduTeacherService.list(null);
        return R.ok().data("items",list);
    }

    //逻辑删除讲师
    @ApiOperation(value="逻辑删除讲师")
    @DeleteMapping("{id}")
    public R removeTeacher(@ApiParam(name = "id",value = "讲师Id",required = true) @PathVariable String id){
        boolean flag =eduTeacherService.removeById(id);
        return flag == true?R.ok():R.error();
    }

    //分页查询讲师的信息
    @ApiOperation(value="讲师分页查询")
    @GetMapping("pageListTeacher/{current}/{limit}")
    public R pageListTeacher(@PathVariable Long current,@PathVariable Long limit){

        Page<EduTeacher> pageParam = new Page<EduTeacher>(current,limit);

        eduTeacherService.pageMaps(pageParam,null);
        List<EduTeacher> records =  pageParam.getRecords();
        Long totals = pageParam.getTotal();
        return R.ok().data("total",totals).data("rows",records);
    }

    //条件查询所有讲师的信息
    @ApiOperation(value="讲师条件分页查询")
    @PostMapping("pageTeacherCondition/{current}/{limit}")
    public R pageListTeacherQuery(@PathVariable Long current ,@PathVariable Long limit ,
                                  @RequestBody(required = false)  teacherQuery teacherQuery){

        Page<EduTeacher> pageParam = new Page<EduTeacher>(current,limit);
        QueryWrapper<EduTeacher> wapper = new QueryWrapper<EduTeacher>();
        String name = teacherQuery.getName();
        String begin =teacherQuery.getBegin();
        String end = teacherQuery.getEnd();
        Integer level = teacherQuery.getLevel();
        if(!StringUtils.isEmpty(name)){
            wapper.like("name",name);
        }
        if(!StringUtils.isEmpty(begin)){
            wapper.ge("gmt_create",begin);
        }
        if(!StringUtils.isEmpty(end)){
            wapper.le("gmt_create",end);
        }
        if(!StringUtils.isEmpty(level)){
            wapper.eq("level",level);
        }

        wapper.orderByDesc("gmt_create");
        eduTeacherService.page(pageParam,wapper);
        List<EduTeacher> records =  pageParam.getRecords();
        Long totals = pageParam.getTotal();
        return R.ok().data("total",totals).data("rows",records);

    }

    //添加讲师
    @ApiOperation(value = "添加讲师")
    @PostMapping("addEduTeacher")
    public R addEduTeacher(@RequestBody EduTeacher eduTeacher){
       boolean flag =  eduTeacherService.save(eduTeacher);
       if(flag) return R.ok();
       else  return  R.error();
    }

    //根据Id查询讲师
    @GetMapping("getTeacherById/{id}")
    public  R  getTeacherById(@PathVariable String id){
        EduTeacher  eduTeacher =  eduTeacherService.getById(id);
        return R.ok().data("item",eduTeacher);
    }

    @PostMapping("updateTeacher")
    public R updateTeacher(@RequestBody EduTeacher eduTeacher){
       return   eduTeacherService.updateById(eduTeacher)?R.ok():R.error();
    }

}

