package com.atguigu.oss.service.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.atguigu.oss.service.OssService;
import com.atguigu.oss.utils.ConstantPropertiesUtils;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

@Service
public class OssServiceImpl implements OssService {
    @Override
    public String uploadFileAvatat(MultipartFile file) {

        String endpoint = ConstantPropertiesUtils.END_POINT;
        String accessKeyId = ConstantPropertiesUtils.ACCESS_KEY_ID;
        String accessKeySecret = ConstantPropertiesUtils.ACCESS_KEY_SECRET;
        String buckName = ConstantPropertiesUtils.BUCKET_NAME;
        try {
            OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
            InputStream  inputStream = file.getInputStream();
            String fileName = file.getOriginalFilename();
            //使文件名变唯一
            String  fileNewName = UUID.randomUUID().toString().replaceAll("-","");

            fileName = fileNewName+fileName;

            String dataPath = new DateTime().toString("yyyy/MM/dd");

            fileName = dataPath+"/"+fileName;

            ossClient.putObject(buckName, fileName, inputStream);
            // 关闭OSSClient。
            ossClient.shutdown();
            //把上传的文件路径进行返回
            String url = "https://"+buckName+"."+endpoint+"/"+fileName;
            return url;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
