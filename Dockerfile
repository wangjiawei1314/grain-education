FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY ./target/gaiguli.jar gaiguli.jar
ENTRYPOINT ["java","-jar","/gaiguli.jar", "&"]